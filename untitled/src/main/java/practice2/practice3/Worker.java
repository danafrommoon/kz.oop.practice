package practice2.practice3;

public abstract class Worker {
    private String name;

    public Worker(String name) {
        setName(name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public abstract void develop();

    public abstract void work();

    public abstract void writeBack();
}
