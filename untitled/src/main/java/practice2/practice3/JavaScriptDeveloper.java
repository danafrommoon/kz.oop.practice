package practice2.practice3;

public class JavaScriptDeveloper extends Worker implements FrontendDeveloper{

    public JavaScriptDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeBack() {

    }

    @Override
    public void CreateDesign() {

    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() +", I am writing Javascript code for Front!");
    }

    @Override
    public void CreateProject() {

    }

    @Override
    public void writeCode() {

    }
}
