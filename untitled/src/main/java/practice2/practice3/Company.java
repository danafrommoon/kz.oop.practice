package practice2.practice3;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees = new ArrayList<>();

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void startWork() {
        employees.forEach(e -> e.work());
    }

    public void addEmployee(JavaDeveloper adilet) {
    }
}
