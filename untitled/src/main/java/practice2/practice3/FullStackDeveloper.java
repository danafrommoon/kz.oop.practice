package practice2.practice3;

public abstract class FullStackDeveloper extends Worker implements FrontendDeveloper,BackendDeveloper{
    public FullStackDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeDB() {

    }

    @Override
    public void writeBack() {
        System.out.println("I am writing back code in Java");
    }




    @Override
    public void CreateDesign() {

    }

    @Override
    public void writeFront() {
        System.out.println("I am writing front in CSS");
    }

    @Override
    public void CreateProject() {

    }

    @Override
    public void writeCode() {

    }
}
