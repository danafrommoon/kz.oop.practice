package practice2.practice3;

public interface Developer extends Person {
    void CreateProject();
    void writeCode();
}
