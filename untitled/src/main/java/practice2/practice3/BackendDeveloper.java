package practice2.practice3;

public interface BackendDeveloper extends Developer {
    void writeDB();
    void writeBack();
}
