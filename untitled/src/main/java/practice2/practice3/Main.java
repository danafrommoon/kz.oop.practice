package practice2.practice3;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        // write your code here
        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Adilet"));
        company.addEmployee(new JavaDeveloper("Dana"));
        company.addEmployee(new JavaScriptDeveloper("Kausar"));
        company.addEmployee(new FullStackDeveloper("Bauyrzhan"));

        company.startWork();
    }
    Worker first = new Worker("John") {
        @Override
        public void develop() {

        }

        @Override
        public void work() {

        }

        @Override
        public void writeBack() {

        }
    };

        //connect with database
        String url = "jdbc:mysqli//localhost:3306/phpmyadmin/jpaoop/";
        String user = "root";
        String password= "";
        try{
            Connection myConn = DriverManager.getConnection(url,user,password);
            Statement myStmt = myConn.createStatement();
            String sql = "select *from jpaoop.company";
            ResultSet rs = myStmt.executeQuery(sql);

            while(rs.next()) {
                System.out.println(rs.getString("name"));

            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

