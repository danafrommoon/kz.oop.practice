package practice2;

public interface Conductor extends Worker {
    void Control();
    void TellTheStemming();
    void NavigatePeople();
}
