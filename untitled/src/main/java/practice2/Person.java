package practice2;

public class Person {
    private int id;
    private static int id_gen = 0;
    public String name;
    public String surname;
    private String position;

    public Person(int id){
        generateId();
    }
    public Person(String name,String surname, String position){
        setName(name);
        setSurname(surname);
        setPosition(position);
    }

    protected static void add(Person p1) {

    }

    private void generateId(){
        id = id_gen;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
