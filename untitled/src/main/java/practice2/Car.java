package practice2;

import java.util.ArrayList;
import java.util.List;

public class Car extends Person {

    public Car(String name, String surname, String position) {
        super(name, surname, position);
    }
    //in car, there can be only 20 person
    private List<Person> people = new ArrayList<Person>();

    public void addEmployee(Person p1) {
        add(p1);
    }

}