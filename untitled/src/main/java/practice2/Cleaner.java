package practice2;

public interface Cleaner extends Worker {
    void CleanPlastcard();
    void CleanCupe();
    void CleanCarriage();

}
