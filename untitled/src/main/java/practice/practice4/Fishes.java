package practice.practice4;

public class Fishes {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String sort;
    public Fishes(int id){
        generateId();
    }
    public Fishes(String name,String sort){
        setName(name);
       setSort(sort);
    }
    private void generateId(){
        id = id_gen;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSort() {
        return sort;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
